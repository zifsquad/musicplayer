package ZIF_Audio_Player;
import java.util.ArrayList;
import java.util.Collections;

public class MusicShuffler{

public static void main(String[] args) 
{
RandomizePL();
}
public static void RandomizePL() 
	{		
		ArrayList<String> playlist = new ArrayList<String>();
		
					// Make this an Integer ArrayList so that is generates random numbers (1 functionality)
					// Put the Song Selected on the top of the playList( 2nd functionality)
		 	 	
		playlist.add("2-DoinJustFine.mp3");
		playlist.add("3-HotelCalifornia.mp3");
		playlist.add("4-BadCompany.mp3");
		playlist.add("5-DontStopBelievin.mp3");
		playlist.add("6-BurnItDown.mp3");
		playlist.add("7-Heartbreaker.mp3");
		playlist.add("8-Tired.mp3");
		playlist.add("9-WorldwideChoppers.mp3");
		playlist.add("10-Roll Up.mp3");

		Collections.shuffle(playlist);
		
		System.out.println("Results after shuffle:");
		System.out.println();
		
		for(String str:playlist)
		{
			System.out.println(str);
		}
		System.out.println();
		System.out.println("The current song playing is " + playlist.get(0) + "!");
	}	
}